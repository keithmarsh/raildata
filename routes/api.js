var Stomp = require('stomp-client');
var util = require('./util.js');
var config = require('../config/settings.json');
var async = require('async');

module.exports = function api_module (app, db) {
    "use strict";
    var mStompClient = null;

    this.feed = function(req, res, next) {
        var destination = '/topic/TRAIN_MVT_FREIGHT';
        var verb = req.params.verb;
        if (mStompClient === null) {
            mStompClient = new Stomp('datafeeds.networkrail.co.uk', 61618, config.feed.user, config.feed.pass);
	        mStompClient.maxWebSocketFrameSize = 64 * 1024;
	        //mStompClient = new Stomp.overWS('ws://datafeeds.networkrail.co.uk:61618/stomp');
        }
        var errorHandler = function ( err ) {
            console.log('feed Error:' + err);
        };
        switch (verb) {
        case 'start':
            var connectHandler = function(sessionId) {
                mStompClient.subscribe(destination, function(messages, headers) {
	                if (typeof messages == 'string') {
		                messages = JSON.parse(messages);
	                }
	                if (! messages[0].body) {
		                console.log('iffy message ' + messages + ' type ' + typeof messages);
		                console.log('iffy object ' + JSON.stringify(messages));
		                return;
	                }
                    async.eachSeries(messages, storeMovementFeed, function(err) {
                        if (err) {
	                        console.log('async.each gave an error:' + err) ;
                        }
                        console.log(messages.length + ' messages processed');
                    });
                });
            }
            mStompClient.connect( connectHandler, errorHandler);
            res.json({'status':'subscribed'});
            break;
        case 'stop':
            mStompClient.disconnect(function() {
                console.log('STOMP disconnected');
                mStompClient = null;
            });
            res.json({'status':'unsubscribed'});
            break;
        default:
            console.log('unknown feed verb ' + verb);
            break;
        };
    };
    
    function storeMovementFeed(message, cb) {
        var _id = message['body']['train_id'];
        if (_id === undefined ||_id.length == 0) return cb();
        switch (message.header.msg_type) {
        case '0003':
	        message.body['actual_timestamp'] = new Date(parseInt(message.body['actual_timestamp']));
	        message.body['planned_timestamp'] = new Date(parseInt(message.body['planned_timestamp']));
        case '0001':
        case '0002':
	        message.body['_id'] = _id;
	        message.body['msg_type'] = message.header.msg_type;
	        message.body['updated'] = new Date();
            db.collection('movements', function(err, movements) {
                if (err) return cb(err);
                movements.update({'_id':_id}, message.body, {'upsert':true, w:0});
            });
            break;
        default:
            console.log('ignored movement type ' + message.header.msg_type);
        }
        cb();
    }

	this.movements = function(req, res, next) {
		db.collection('movements', function(err, movements) {
			if (err) return next(err);
			movements.find().sort({_id:1}).toArray(function(err,documents) {
				if (err) return next(err);
				res.json(documents);
			});
		});
	}

	this.getStanox = function(req, res, next) {
		var tiplocLatLng = {};
		db.collection('naptanraw', function(err, naptan) {
			if (err) return next(err);
			naptan.find(
				{"stopclassification.offstreet.rail.annotatedrailref.tiplocref":{$exists:1}},
				{"stopclassification.offstreet.rail.annotatedrailref.tiplocref":1,"latlng":1
				}).each (function(err,naptan) {
				if (err) return next(err);
				if (naptan != null) {
					tiplocLatLng[naptan.stopclassification.offstreet.rail.annotatedrailref.tiplocref] = naptan.latlng;
				} else {
					db.collection('TiplocV1', function(err, movements) {
						if (err) return next(err);
						var stanox = {}
						movements.find().each( function ( err, item) {
							if (err) return next(err);
							if (item == null) {
								return res.json(stanox);
							}
							if (item.stanox != null && item.tps_description != null) {
								stanox[item.stanox] = {};
								stanox[item.stanox].description = item.tps_description;
								if (tiplocLatLng[item.tiploc_code] !== undefined) {
									stanox[item.stanox].latlng = tiplocLatLng[item.tiploc_code];
								}
							}
						});
					});
				}
			});
		});
	}

	this.getCorpus = function(req, res, next) {
		util.getBuffer('datafeeds.networkrail.co.uk', '/ntrod/SupportingFileAuthenticate?type=CORPUS',
			config.feed.user, config.feed.pass, function onResult(err, buffer) {
				if (err) return next(err);
				console.log(buffer.slice(0,20));
				var docs = JSON.parse(buffer.toString('UTF-8'))['TIPLOCDATA'];
				buffer = null;
				// Set NLC as _id

				for (var ix = 0, l = docs.length; ix < l; ix++) {
					docs[ix]['_id'] = docs[ix]['NLC'];
				}
				db.collection('CORPUS', function(err, collection) {
					if (err) return next(err);
					collection.drop();
					collection.insert(docs, { w:0 } );
				});
				res.json({count:docs.length});
			});
	};

	this.getSmart = function(req, res, next) {
		util.getBuffer('datafeeds.networkrail.co.uk', '/ntrod/SupportingFileAuthenticate?type=SMART',
			config.feed.user, config.feed.pass, function onResult(err, buffer) {
				if (err) return next(err);
				var docs = JSON.parse(buffer.toString('UTF-8'));
				buffer = null;
				db.collection('SMART', function(err, collection) {
					if (err) return next(err);
					// Hmmm
					collection.drop();
					collection.insert(docs.BERTHDATA, { w:0 } );
					console.log("Inserted " + docs.BERTHDATA.length);
				});
				res.json(docs.BERTHDATA);
			});
	};

	this.getSmartTD = function(req, res, next) {
		console.log('getSmartTD');
		db.collection('SMART', function(err, collection) {
			if (err) return next(err);
			var tdArr = req.params.td.split(',');
			console.log(tdArr);
			collection.find({TD:{'$in':tdArr},STEPTYPE:'B'}).toArray(function(err, links) {
                res.json(links);
			});
		});
	};

	this.getSmartTDs = function(req, res, next) {
		db.collection('SMART', function(err, smart) {
            smart.count(function(err, total) {
                console.log("SMART.count: " + total);
            });
			if (err) return next(err);
			var match = { $group : { _id : '$TD', count: { $sum : 1 } } };
			var sort = { $sort : { _id : 1 } };
			smart.aggregate( [ match, sort ], function ( err, arr ) {
				res.json(arr);
			});
		});
	}

    
    this.getSchedule = function(req, res, next) {
        util.getBuffer('datafeeds.networkrail.co.uk', '/ntrod/CifFileAuthenticate?type=CIF_HY_TOC_FULL_DAILY&day=toc-full',
        config.feed.user, config.feed.pass, function onResult(err, buffer) {
            if (err) return next(err);
            // Lets iterate over each object and insert into an appropriate table
            var braceCount = 0;
            var chIx = 0;
            var len = buffer.length;
            var startIx = 0;
            var started = false;
            var docCount = 0;
            while (chIx < len) {
                started = false;
                while ((!started || braceCount > 0) && chIx < len) {
                    var ch = buffer[chIx++];
                    if (ch == '{') {
                        if (braceCount === 0) {
                            startIx = chIx - 1;
                            started = true;
                        }
                        braceCount++;
                    }
                    if (ch == '}') {
                        braceCount--;
                    }
                }
                var document = JSON.parse(buffer.substring(startIx, chIx));
                for (var key in document) {
                    var insertDoc = document[key];
                    var ignore = false;
                    if (key == 'JsonScheduleV1') {
                        insertDoc['_id'] = insertDoc['CIF_train_uid'];
                    }
                    else if (key == 'TiplocV1') {
                        insertDoc['_id'] = insertDoc['tiploc_code'];
                    }
                    else if (key == 'JsonAssociationV1') {
                        insertDoc['_id'] = insertDoc['main_train_uid'];
                    }
                    else {
                        console.log('ignore key ' + key + ':' + document);
                        ignore = true;
                    }

                    db.collection(key, function(err, collection) {
                        if (err) return next(err);
                        if (!ignore) {
                            collection.update({
                                '_id': insertDoc['_id']
                            }, insertDoc, {
                                upsert: true,
                                w: 0
                            });
                        }
                    });
                }
                docCount++;
            }
            res.json({count: docCount});
        });
    };
}