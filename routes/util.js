var https = require('https');
var url = require('url');
var zlib = require('zlib');

exports.getBuffer = function(host, uri, user, pass, onResult) {
    "use strict";
	console.log('getJSON(' + host + ')');
	var options = {
		hostname : host,
		path :     uri,
		method :   'GET'
	};
	if ( user !== null ) {
		options.headers = { 'Authorization' : 'Basic ' + new Buffer(user + ':' + pass).toString('base64') };
	}
	var output = [];
	https.globalAgent.options.secureProtocol = 'TLSv1_method';
	var request = https.request(options, function(response) {
		if ( response.statusCode == 302 ) {
			var goto = url.parse(response.headers.location);
			console.log('redirect to ' + goto.hostname);
			exports.getBuffer(goto.hostname, goto.path, null, null, onResult);
			return;
		}
		console.log("response code is " + response.statusCode);
		console.log("headers are " + JSON.stringify(response.headers, null, 2));
		response.on('data', function(chunk) {
			output.push(chunk);
		});
		response.on('end', function() {
			var buffer = Buffer.concat(output);
			switch ( response.headers['content-type'] )
			{
			case 'application/x-gzip':
			case 'application/octet-stream':
				{
					console.log("length " + buffer.length);
					zlib.gunzip(buffer, function callback(err, result) {
						if (err) return onResult(err);
						onResult(null, result.toString('UTF-8'));
					});
				}
				break;
			default:
				{
					var obj = null;
					try {
						obj = buffer.toJSON();
						onResult(null, obj);
					} catch ( e ) {
						onResult(e, null);
					}
				}
			}
		});
	});
	request.on('error', function(err) {
		onResult(err, null);
	});
	request.end();
};

    