var sax = require('sax');
var http = require('http');
var unzip = require('unzip');

module.exports = function naptan_module (app, db) {
	"use strict";

	var fetchOptions = {
		hostname : 'www.dft.gov.uk',
		path : '/NaPTAN/snapshot/NaPTANxml.zip',
		method : 'GET'
	};
	var saxExpects = {
		stoppoint : [ 'atcocode', 'naptancode', 'descriptor', 'place', 'stopclassification', 'administrativearearef' ],
		descriptor : [ 'commonname','street' ],
		place : [ 'suburb', 'town', 'localitycentre', 'location' ],
		location : [ 'translation' ],
		translation : [ 'gridtype','easting','northing','longitude','latitude' ],
		stopclassification : [ 'stoptype', 'offstreet' ],
		offstreet : [ 'rail' ],
		rail : [ 'annotatedrailref' ],
		annotatedrailref : [ 'tiplocref','crsref','stationname' ]
	};

	this.get = function(req, res, next) {
		var saxStream = sax.createStream();

		http.request(fetchOptions, function(response) {
			response
				.pipe(unzip.Parse())
				.on('entry', function (entry) {
					console.log('entry ' + JSON.stringify(entry));
					if (entry.path === "NaPTAN.xml") {
						//entry.pipe(ofs);
						entry.pipe(saxStream);
					} else {
						console.log("skip");
						entry.autodrain();
					}
				})
				.on('end', function() {
					console.log('response ended');
					res.json({status:'done'});
				});
		}).end();

		var saxStack = [];
		var saxHit = false;
		var stop = {};

		saxStream.on("opentag", function(node) {
			var nodename = node.name.toLowerCase();
			saxHit = false;
			// Look for me or my Children or my parents
			if (saxStack.length === 0) {
				if (saxExpects[nodename]) {
					saxHit = true;
					saxStack.push(nodename);
				}
				return;
			}
			for (var ix = saxStack.length-1; ix >= 0; ix--) {
				var nameOnTheStack = saxStack[ix];
				if (nameOnTheStack === undefined) {
					continue;
				}
				var kidsOnTheStack = saxExpects[nameOnTheStack];
				// Am I the same name?
				if (nameOnTheStack === nodename) {
					if (ix == 0) {
						processStop(stop);
						stop = {};
					}
					saxHit = true;
					while ( saxStack.length-1 > ix) {
						saxStack.pop();
					}
					// Yes, no change
					return;
				} else {
					// Am I one of the children
					if (kidsOnTheStack !== undefined) {
						for (var iy = kidsOnTheStack.length; iy >= 0; iy--) {
							if (nodename === kidsOnTheStack[iy]) {
								// Found a child.  Add to stack
								saxHit = true;
								while ( saxStack.length-1 > ix) {
									saxStack.pop();
								}
								return saxStack.push(nodename);
							}
						}
					}
				}
			}
		});
		/**
		 * a.b.c.d.text from saxStack['a','b',c','d']
		 */
		saxStream.on('text', function(text) {
			if (!saxHit) {
				return;
			}
			var obj = stop;
			for (var ix = 0, len = saxStack.length; ix < len; ix++) {
				var propName = saxStack[ix].toLowerCase();
				if (!obj[propName]) {
					obj[propName] = {};
				}
				if (ix + 1 < len) {
					obj = obj[propName];
				} else {
					obj[propName] = text;
				}
			}
		});
	}

    this.count = function(req, res, next) {
        db.collection('naptan', function(err, collection) {
            if (err) return next(err);
            collection.count(function(err,count) {
                if (err) return next(err);
                res.json({"count":count});
            });
        });

    }

	function processStop(station, next) {
		if (station.stoppoint.stopclassification.stoptype !== undefined && station.stoppoint.stopclassification.stoptype.charAt(0) === 'R') {
			var latlong = {
				type : "Point",
				coordinates : [ station.stoppoint.place.location.translation.longitude, station.stoppoint.place.location.translation.latitude ]
			};
			var stationDoc = {
				_id : station.stoppoint.atcocode,
				commonname : station.stoppoint.descriptor.commonname,
				latlng : latlong,
				stoptype: station.stoppoint.stopclassification.stoptype
			};
			db.collection('naptan', function(err, collection) {
				if (err) return next(err);
				collection.update({	'_id': stationDoc['_id'] }, stationDoc, { upsert: true,	w: 0 });
				console.log(JSON.stringify(stationDoc));
			});
			station.stoppoint.latlng = latlong;
			station.stoppoint._id = station.stoppoint.atcocode;
			db.collection('naptanraw', function(err, collection) {
				if (err) return next(err);
				collection.update({	'_id': stationDoc['_id'] }, station.stoppoint, { upsert: true,	w: 0 });
			});
		}
	}
}
