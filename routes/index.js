var api_module = require('./api');
var naptan_module = require('./naptan');

module.exports = function(app, db) {
    "use strict";

    //var sessionHandler = new SessionHandler(db);
    //var contentHandler = new ContentHandler(db);
    // Middleware to see if a user is logged in
    //app.use(sessionHandler.isLoggedInMiddleware);
    var api = new api_module(app, db);
    var naptan = new naptan_module(app, db);

	app.get('/api/smart', api.getSmart);
	app.get('/api/td/:td', api.getSmartTD);
	app.get('/api/tds', api.getSmartTDs);
	app.get('/api/corpus', api.getCorpus);
    app.get('/api/schedule', api.getSchedule);
    app.get('/api/feed/:verb', api.feed);
	app.get('/api/movements', api.movements);
	app.get('/api/stanox', api.getStanox);
	app.get('/api/naptan', naptan.get);
    app.get('/api/naptan/count', naptan.count);

    app.use(function(err, req, res, next) {
       "use strict";
        console.error("KJM " + err.message);
        console.error("KJM " + err.stack);
        res.status(500);
        res.json(err);
    });
}


