var express = require('express');
var path = require('path');
var routes = require('./routes');
var mongo = require('mongodb');
var os = require('os');
var config = require ('./config/settings.json');
var app = express();

// Use localhost mongo unless cloud9 or openshift
var mongoURL = 'mongodb://localhost:27017/raildata';
if (process.env.OPENSHIFT_NODEJS_IP || process.env.IP) {
    mongoURL = 'mongodb://' + config.db.user + ':' + config.db.pass + '@ds053788.mongolab.com:53788/raildata';
}
mongo.MongoClient.connect(mongoURL, null, function(err, db) {
    "use strict";
    if (err) throw err;

    var interfaces = os.networkInterfaces();
    var networkIP = "127.0.0.1";
found:
    for (var inf in interfaces) {
        for (var addr in interfaces[inf]) {
            var address = interfaces[inf][addr];
            if (address.family == 'IPv4' && !address.internal) {
                networkIP = address.address;
                break found;
            }
        }
    }
    app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080);
    app.set('address', process.env.OPENSHIFT_NODEJS_IP || process.env.IP || networkIP );
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.json());
    //app.use(express.urlencoded());
    //app.use(express.bodyParser());
    //app.use(express.cookieParser('Too few LEDs are dim'));
    //app.use(express.methodOverride());
    //app.use(express.session());
    app.use(express.static(path.join(__dirname, 'client')));

    if ('development' == app.get('env')) {
        app.use(express.errorHandler());
	    app.use(express.static(path.join(__dirname, 'client_test')));
    }

    routes(app, db);

    app.listen(app.get('port'), app.get('address'), function() {
        console.log("Express server listening on " + app.get('address') + ":" + app.get('port') + " in mode: " + app.settings.env);
    });
});
