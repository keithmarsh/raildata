'use strict';

/* jasmine specs for services go here */

describe('NaPTAN service', function () {
	var mockNaptanService, $httpBackend;

	beforeEach(function () {
		angular.mock.module('raildata-services');
		angular.mock.inject(function ($injector) {
			$httpBackend = $injector.get('$httpBackend');
			mockNaptanService = $injector.get('naptanService');
		})
	});

	describe('count', function () {
		it('should call count', inject(function () {
			$httpBackend.expectGET('api/naptan/count')
				.respond(200, {count: 62	});

			var result = mockNaptanService.count();
			$httpBackend.flush();
			expect(result.count).toEqual(62);
		}));
		it('should handle missing database', inject(function() {
			$httpBackend.expectGET('api/naptan/count')
				.respond(500, new Error('no database'));
			var result = mockNaptanService.count();
			$httpBackend.flush();
			expect(result).not.toBeDefined();


		}));
	});
});