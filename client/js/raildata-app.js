"use strict";
var app = angular.module('raildata-app', ['ngRoute', 'ngAnimate', 'raildata-services']);

app.config(function($routeProvider) {
	$routeProvider
		.when( '/',       { templateUrl: 'partial/tds.html',   controller: "tds-ctrl" } )
		.when( '/td/:td', { templateUrl: 'partial/td.html',   controller: "td-ctrl" } )
		.when( '/feed',   { templateUrl: 'partial/feed.html', controller: 'feed-ctrl'} )
		.when( '/smart',  { templateUrl: 'partial/smart.html', controller: "smart-ctrl" } )
		.when( '/schedule',{ templateUrl: 'partial/schedule.html', controller: "schedule-ctrl" } )
		.when( '/naptan' ,{ templateUrl: 'partial/naptan.html', controller: "naptan-ctrl" } );
});

/**
 * F e e d
 */
app.controller('feed-ctrl', function($scope, $timeout, feedService) {
	feedService.stanox(function (data) {
		$scope.stanox = data;
	});
    $scope.start = function() {
        feedService.start();
    }    
    $scope.stop = function() {
        feedService.stop();
    }
    $scope.data = [];
    $scope.tickTimer = function() {
	    feedService.movements(function (trains) {
		    var sIx = 0;
		    for (var tIx = 0,l = trains.length; tIx < l; tIx++,sIx++) {
			    // Added to the end
			    if (tIx >= $scope.data.length) {
				    $scope.data.push(trains[tIx]);
				    $scope.data[sIx].ne = 0;
			    } else {
					if ($scope.data[sIx].updated != trains[tIx].updated) {
						// A train has been updated or its a new one
						if (trains[tIx].train_id == $scope.data[sIx].train_id) {
							// It's been updated
							var count = $scope.data[sIx].ne + 1;
							$scope.data[sIx] = trains[tIx];
							$scope.data[sIx].ne = count;
						} else {
							// It's a new one
							$scope.data.splice(sIx, 0, trains[tIx]);
							$scope.data[sIx].ne = 0;
						}
					}
			    }
			    var stanox = $scope.stanox[$scope.data[sIx].loc_stanox];
			    if (stanox !== undefined) {
			        $scope.data[sIx].stanox = stanox.description;
			        $scope.data[sIx].latlng = stanox.latlng;
				    if (stanox.latlng !== undefined) {
				        if ($scope.data[sIx].ne == 0 || $scope.data[sIx].marker === undefined) {
					        $scope.data[sIx].marker = new google.maps.Marker({
						        position : new google.maps.LatLng(stanox.latlng.coordinates[1],stanox.latlng.coordinates[0]),
					            map:$scope.map,
						        title:$scope.data[sIx]._id
				            });
				        } else {
					        console.log("animate!");
					        $scope.data[sIx].marker.position = new google.maps.LatLng(stanox.latlng.coordinates[1],stanox.latlng.coordinates[0]);
				        }
			        }
			    }
		    }
		    //$scope.data = trains;
		    //console.log('KJM:'+JSON.stringify(x));
	    });
        $scope.startTimer();
    }
    $scope.startTimer = function() {
        $timeout($scope.tickTimer, 5000);
    };
	$scope.mapInit = function() {
		var mapOptions = {
			center: new google.maps.LatLng(55.8536853379,-4.018641637),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		$scope.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	};

});

/**
 * T D S
 */
app.controller('raildata-ctrl', function($scope, tds) {
    $scope.data = tds.query();
});

/**
 * S c h e d u l e
 */
app.controller('schedule-ctrl', function($scope, scheduleService) {
    $scope.data = scheduleService.query();
});

/**
 * N a P T A N
 */
app.controller('naptan-ctrl', function($scope, $timeout, naptanService) {
	$scope.loading = false;
    $scope.count = naptanService.count();
    $scope.tickTimer = function() {
        $scope.count = naptanService.count();
        $scope.startTimer();
    }
    $scope.startTimer = function() {
        $timeout($scope.tickTimer, 5000);
    };
    $scope.fetch = function() {
        console.log('fetch');
	    $scope.loading = true;
        //$('#arse').button('loading');
        //TODO
        // $scope.data = naptanService.query(function ())
    }
});

/**
 * S M A R T
 */
app.controller('smart-ctrl', function($scope, $http) {
	$http.get('/api/smart')
		.success(function(data) {
			$scope.data = data;
		})
		.error(function(data, status) {
			console.log("$http.get error " + status + " : " + data);
		});
});

app.controller('tds-ctrl', function($scope, $http, $location) {
	console.log('route: tds');
	$http.get('/api/tds')
		.success(function(tds) {
			$scope.tds = tds;
		})
		.error(function(data, status) {
			console.log("$http.get /api/tds error " + status + " : " + data);
		});
	$scope.included = [];
	$scope.go = function() {
		var a=[];
		if ($scope.tds !== undefined) {
			console.log("in");
			for (var ix=0,l=$scope.tds.length; ix < l; ix++) {
				if ($scope.tds[ix].include) {
					a.push($scope.tds[ix]._id);
				}
			}
			console.log("a="+a);
		}
		$location.path("/td/" + a);
	};

});

app.controller('td-ctrl', function($scope, $http, $routeParams) {
	$http.get('/api/td/' + $routeParams.td)
		.success(function(dbLinks) {
			$scope.data = dbLinks;
			var nodeNames = {};
			var nodes = [];
			var links = [];
			// Get the unique set of names
			for ( var i=0,l=dbLinks.length; i < l; i++) {
                var link = dbLinks[i];
                nodeNames[link.TOBERTH] = { group:link.STANME };
                nodeNames[link.FROMBERTH] = { group:link.STANME };
			}
            var ix = 0;
            for ( var nodeName in nodeNames ) {
                nodes[ix] = { name:nodeName + ":" + nodeNames[nodeName].group, group:nodeNames[nodeName].group };
                nodeNames[nodeName].ix = ix++;
            }
			// For each link from the db, add to the links array
            for (i=0,l=dbLinks.length; i < l; i++) {
                var link2 = dbLinks[i];
	            if (link2.FROMBERTH.length > 0 && link2.TOBERTH.length > 0) {
                    links.push ( { source: nodeNames[link2.FROMBERTH].ix, target: nodeNames[link2.TOBERTH].ix, value:2 } );
	            }
            }
            var graph = { nodes: nodes, links: links }
            $scope.graph = graph;
            doTheGraph(graph);
		})
		.error(function(data, status) {
			console.log("$http.get error " + status + " : " + data);
		});
});
app.controller('corpus-ctrl', function($scope, $http) {
	$http.get('/api/corpus')
		.success(function(data) {
			$scope.data = data;
		})
		.error(function(data, status) {
			console.log("$http.get error " + status + " : " + data);
		});
});
app.controller('schedule-ctrl', function($scope, $http) {
    $http.get('/api/schedule')
        .success(function(data) {
            $scope.data = data;
        })
        .error(function(data, status) {
            console.log("$http.get error " + status + " : " + data);
        });
});



