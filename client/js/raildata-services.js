var services = angular.module('raildata-services', ['ngResource']);

// See http://docs.angularjs.org/api/ngResource.$resource
// $resource(url, paramDefaults, actions)
// actions ::= { action1: { method:xx, params:xx, isArray:xx, headers:xx}, action2... }

/**
 * T D S
 */
services.factory('tds', [ '$resource', function($resource) {
    return $resource('api/tds');
}]);

/**
 * F e e d
 */
services.factory('feedService', [ '$resource', function($resource) {
    return $resource('api/feed/:verb', {},
        {'start':{method:'GET',params:{verb:'start'}},
         'stop' :{method:'GET',params:{verb:'stop'}},
         'movements':{method:'GET',url:'api/movements',isArray:true,cache:false},
         'stanox':{method:'GET',url:'api/stanox'}});
}]);

/**
 * S c h e d u l e
 */
services.factory('scheduleService', [ '$resource', function($resource) {
    return $resource('api/schedule');
}]);

/**
 * N a P T A N
 */
services.factory('naptanService', [ '$resource', function($resource) {
	return $resource('api/naptan', {},
        {'count':{method:'GET',url:'api/naptan/count'}});
}]);
